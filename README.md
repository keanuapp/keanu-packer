# keanu-packer

This project contains the infra for building golden AMIs for Keanu.

To learn how to use this repository, head to
[docs.keanu.im](http://docs.keanu.im).


### Images built:

* synapse
* mxisd
* sygnal

## How to build

More information is available at [docs.keanu.im](https://docs.keanu.im)

### Local dependencies

Ensure you have these local deps installed:

* [Ansible](https://www.ansible.com/), with ansible-galaxy
* [Packer](https://www.packer.io/)
* make
* aws-vault

### Build


1. You'll need to set your profiles variable in `.env` to contain
   space-delimited list of all your aws-vault profiles that you will be using
   to build AMIs. An example is in `.env.sample`.

2. You can also configure the software versions in `.env`, see `.env.sample`

3. Execute a build with : `make` or a single ami by specifying the profile and service,
   e.g., `make your-aws-profile/synapse`

# License

All code in this repo is licensed under the [GNU Affero General Public License
(AGPL) v3+](https://www.gnu.org/licenses/agpl-3.0.en.html), unless other wise
stated.

# Author Information

* Abel Luck <abel@guardianproject.info> of [Guardian Project](https://guardianproject.info)
