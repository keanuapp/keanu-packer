.SECONDEXPANSION:
include .env
app := mxisd synapse sygnal
obj := $(foreach X,$(profiles),$(foreach Y,$(app),$X/$Y))
obj_all := $(foreach X,$(profiles),$X/all)
.PHONY: all $(obj) $(app) $(profiles)


all: $(obj_all)

$(app): $(foreach X,$(profiles),$X/$$@)

$(obj_all):
	@unset ANSIBLE_VAULT_PASSWORD_FILE && aws-vault exec $(@D) -- packer build packer.json

$(obj):
	unset ANSIBLE_VAULT_PASSWORD_FILE && aws-vault exec $(@D) -- packer build -only=$(@F) packer.json

$(profiles):
	@unset ANSIBLE_VAULT_PASSWORD_FILE && aws-vault exec $@ -- packer build packer.json
